# Meteostation project

The Meteostation project consists of the following repos:
* [Meteologger](https://codeberg.org/LordFilu/meteologger) based on an esp32-dev-board.
* [Meteodaemon](https://codeberg.org/LordFilu/meteodaemon) running on a server for collecting data into an SQLite-db sent from the meteostation/meteologger.
* [Sensornetlib](https://codeberg.org/LordFilu/sensornetlib), the communication-lib between meteologger and meteodaemon (this repo).
* [Meteoserver](https://codeberg.org/LordFilu/meteoserver), a small webserver fetching data from the sqlite-db and serving it on a webpage.

This repo is about

## Sensornetlib
A lib to send the meteodata from the meteologger to the meteodaemon. The data is sent in binary format and thus with a very low traffic-footprint.

### Libraries
Sensornetlib uses the following libraries/frameworks:
* none

### Licence
Copyright (C) 2024 by LordFilu of SwordLord - the coding crew. Please see enclosed LICENCE file for details.