// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor


#include "MessageBroker.hpp"
#include "impl/TCPConnectorPosix.hpp"
#include "SensorNetMessage.hpp"

#include <thread>
#include <iostream>
#include <cstring>
#include <chrono>

namespace snl
{
    MessageBroker::MessageBroker(std::unique_ptr<snl::TCPStream> stream, std::shared_ptr<SnlLog> log) : _stream(std::move(stream)), _log(log)
    {
        _stream->setReceiveTimeout(30);
        _stream->setSendTimeout(30);

    }

    std::unique_ptr<th_protocol> MessageBroker::receiveSensorData()
    {
        _notAllDataReceived = false;

        for (uint8_t i = 0; i < 3; i++)
        {
            std::unique_ptr<SensorNetMessage> snm = receiveSNM();

            if (snm != nullptr)
            {
                std::unique_ptr<th_protocol> thp = snm->evalToThp();
                if (thp != nullptr)
                {
                    if (_log != nullptr) { _log->info("Valid thp received\n"); }
                    sendCtrlSNM(SensorNetMessage::SNMOK);
                    if (_log != nullptr) { _log->info("SNMOK sent\n"); }

                    return thp;
                }
            }

            if (_stream->disconnected())
            {
                break;
            }

            if (_log != nullptr) { _log->warn("requesting resend SNM\n"); }
            sendCtrlSNM(SensorNetMessage::RESENDDATAMSG);

        }

        return nullptr;

    }

    bool MessageBroker::sendSensorData(std::unique_ptr<th_protocol> thp)
    {
        std::unique_ptr<SensorNetMessage> snm = std::unique_ptr<SensorNetMessage>(new SensorNetMessage(SensorNetMessage::DATAMSG));
        snm->evalToSNM(std::move(thp));

        for (uint8_t i = 0; i < 3; i++)
        {
            if (sendSNM(std::move(snm)))
            {
                if (_log != nullptr) { _log->info("DATA SNM sent\n"); }
                std::unique_ptr<SensorNetMessage> snm_resp = receiveSNM();
                if (snm_resp == nullptr)
                {
                    if (_log != nullptr) { _log->error("error occurred while waiting for SNMOK\n"); }
                }
                else if (snm_resp->typeEqualTo(SensorNetMessage::SNMOK))
                {
                    if (_log != nullptr) { _log->info("SNMOK received\n"); }
                    return true;
                }
                else if (snm_resp->typeEqualTo(SensorNetMessage::RESENDDATAMSG))
                {
                    if (_log != nullptr) { _log->warn("RESENDDATAMSG requesting\n"); }
                }

            }

        }

        return false;
    }


    std::unique_ptr<SensorNetMessage> MessageBroker::receiveSNM()
    {
        std::unique_ptr<SensorNetMessage> snm = std::unique_ptr<SensorNetMessage>(new SensorNetMessage());

        ssize_t rec_size = _stream->receive(snm->data(), snm->header_length());
        if (rec_size != snm->header_length())
        {
            return nullptr;
        }

        if (!snm->decode_header())
        {
            if (_log != nullptr) { _log->error("error decoding header. _body_length > MAX_BODY_LENGTH\n"); }
            return nullptr;
        }

        if (snm->body_length() > 0)
        {
            rec_size = _stream->receive(snm->body(), snm->body_length());

            if (rec_size != snm->body_length())
            {
                _notAllDataReceived = true;
                return nullptr;
            }
        }

        return snm;
    }

    bool MessageBroker::sendSNM(std::unique_ptr<SensorNetMessage> snm)
    {
        ssize_t send_size = _stream->send(snm->data(), snm->length());
        return send_size == snm->length();
    }

    void MessageBroker::sendCtrlSNM(const uint8_t (&type)[2])
    {
        if ((type[0] == SensorNetMessage::RESENDDATAMSG[0] && type[1] == SensorNetMessage::RESENDDATAMSG[1])
        || (type[0] == SensorNetMessage::SNMOK[0] && type[1] == SensorNetMessage::SNMOK[1])
        || (type[0] == SensorNetMessage::REQUESTDATA[0] && type[1] == SensorNetMessage::REQUESTDATA[1])
           )
        {
            SensorNetMessage snm(type);
            snm.body_length(0);
            snm.encode_header();

            _stream->send(snm.data(), snm.length());

        }
    }

    bool MessageBroker::disconnected()
    {
        return _stream->disconnected();
    }


}