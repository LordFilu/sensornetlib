// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#pragma once

#include "TCPStream.hpp"
#include "protocol.hpp"

#include <cstdint>
#include <memory>

namespace snl
{
    class MessageBroker
    {
    public:
        MessageBroker(std::unique_ptr<snl::TCPStream> stream, std::shared_ptr<SnlLog> log = nullptr);

        std::unique_ptr<th_protocol>    receiveSensorData();
        bool                            sendSensorData(std::unique_ptr<th_protocol> thp);
        bool                            disconnected();
        bool                            notAllDataReceived() const { return _notAllDataReceived; }

    private:
        bool sendSNM(std::unique_ptr<SensorNetMessage> snm);
        std::unique_ptr<SensorNetMessage> receiveSNM();

        void sendCtrlSNM(const uint8_t (&type)[2]);

        std::unique_ptr<snl::TCPStream> _stream{nullptr};
        std::shared_ptr<SnlLog> _log{nullptr};

        bool _notAllDataReceived{false};
    };

}

