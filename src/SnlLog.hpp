// (c) 2023 by SwordLord - the coding crew
// sensor logger project

#pragma once

#include <memory>
#include <string_view>

namespace snl
{
    class SnlLog
    {
    public:
        virtual void info(const char *__restrict txt, ...) = 0;
        virtual void warn(const char *__restrict txt, ...) = 0;
        virtual void error(const char *__restrict txt, ...) = 0;
//        virtual void info(std::string_view entry) = 0;
//        virtual void warn(std::string_view entry) = 0;
//        virtual void error(std::string_view entry) = 0;
        virtual ~SnlLog() = default;
    };

}
