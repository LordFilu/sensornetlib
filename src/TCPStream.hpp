// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#pragma once

#include "SensorNetMessage.hpp"
#include "SnlLog.hpp"

#include <string>
#include <netinet/in.h>
#include <sys/socket.h>

namespace snl
{
    class TCPStream
    {
    public:

        ~TCPStream();

        void    setSendTimeout(time_t timeout_sec);
        void    setReceiveTimeout(time_t timeout_sec);
        ssize_t send(const uint8_t * buffer, size_t len);
        ssize_t receive(uint8_t * buffer, size_t len);
        std::string readStringUntil(char token);

        bool available();

        std::string getPeerIP();
        int    getPeerPort();

        bool disconnected();

/*        enum {
            connectionClosed = 0,
            connectionReset = -1,
            connectionTimedOut = -2
        };
*/
    private:
        friend class TCPAcceptor;
        friend class TCPConnector;

        TCPStream(int sd, sockaddr_in& address, std::shared_ptr<SnlLog> log);
        TCPStream() = default;
        TCPStream(const TCPStream& stream) = default;

        int             _sd{0};
        std::string     _peerIP{""};
        int             _peerPort{0};

        bool            _disconnected{false};
        std::shared_ptr<SnlLog> _log{nullptr};
    };

}

