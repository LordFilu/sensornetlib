// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#include "SensorNetMessage.hpp"

#include <cstring>

namespace snl
{
    const uint8_t SensorNetMessage::REQUESTDATA[2] = {'D', 'R'};
    const uint8_t SensorNetMessage::DATAMSG[2] = {'D', 'M'};
    const uint8_t SensorNetMessage::RESENDDATAMSG[2] = {'R', 'M'};
    const uint8_t SensorNetMessage::SNMOK[2] = {'S', 'O'};

    SensorNetMessage::SensorNetMessage() : _body_length(0)
    {
        _type[0] = 0;
        _type[1] = 0;
    }

    SensorNetMessage::SensorNetMessage(uint8_t t1, uint8_t t2) : _body_length(0)
    {
        _type[0] = t1;
        _type[1] = t2;
    }

    SensorNetMessage::SensorNetMessage(const uint8_t (&type)[2]) : _body_length(0)
    {
        _type[0] = type[0];
        _type[1] = type[1];
    }

    void SensorNetMessage::body_length(std::size_t new_length)
    {
        _body_length = new_length;
        if (_body_length > MAX_BODY_LENGTH)
        _body_length = MAX_BODY_LENGTH;
    }

    bool SensorNetMessage::decode_header()
    {
        memcpy(&_type, _data, HEADER_TYPE_LENGTH);

        memcpy(&_body_length, _data + HEADER_TYPE_LENGTH, HEADER_SIZE_LENGTH);
        if (_body_length > MAX_BODY_LENGTH)
        {
            _body_length = 0;
            return false;
        }

        return true;
    }

    void SensorNetMessage::encode_header()
    {
        std::memcpy(_data, &_type, HEADER_TYPE_LENGTH);
        std::memcpy(_data + HEADER_TYPE_LENGTH, &_body_length, HEADER_SIZE_LENGTH);
    }

    bool SensorNetMessage::typeEqualTo(char t1, char t2) const
    {
        return (_type[0] == t1 && _type[1] == t2);
    }

    bool SensorNetMessage::typeEqualTo(const uint8_t (&tp)[2]) const
    {
        return (_type[0] == tp[0] && _type[1] == tp[1]);
    }

    std::string SensorNetMessage::message() const
    {
        return std::string((char*)body(), body_length());
    }

    void SensorNetMessage::clear()
    {
        memset(_data, 0, sizeof(_data));
    }

    std::unique_ptr<th_protocol> SensorNetMessage::evalToThp()
    {
        if (typeEqualTo(DATAMSG))
        {
            std::unique_ptr<th_protocol> thp = std::unique_ptr<th_protocol>(new th_protocol());
            std::memcpy(thp.get(), body(), sizeof(snl::th_protocol));
            return thp;
        }

        return nullptr;
    }

    void SensorNetMessage::evalToSNM(std::unique_ptr<th_protocol> thp)
    {
        if (typeEqualTo(DATAMSG))
        {
            body_length(sizeof(snl::th_protocol));
            std::memcpy(body(), thp.get(), body_length());
            encode_header();
        }
    }


}