// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor


#pragma once

#include "TCPStream.hpp"

#include <memory>

namespace snl
{
    class TCPConnector
    {
    public:
        explicit TCPConnector(std::shared_ptr<SnlLog> log = nullptr);
        virtual std::unique_ptr<TCPStream> connectToIP(const std::string& ip, uint16_t port, uint32_t timeout = 0) = 0;
        virtual std::unique_ptr<TCPStream> connectToHost(const std::string& hostname, uint16_t port, uint32_t timeout = 0) = 0;
    protected:
        static std::unique_ptr<TCPStream> createTCPStream(int sd, sockaddr_in& address, std::shared_ptr<SnlLog> log);
        std::shared_ptr<SnlLog> _log;

    };

}

