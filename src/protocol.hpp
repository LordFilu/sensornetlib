// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#pragma once

#include <cstdint>

namespace snl
{
    enum TH_CTRL : uint8_t
    {
        VALID = 0,
        T_ERR,
        H_ERR,
        TH_ERR
    };

    typedef struct th_protocol
    {
        uint16_t year;      // 2
        uint8_t month;      // 1
        uint8_t day;        // 1
        uint8_t hour;       // 1
        uint8_t minute;     // 1
        float temperature;  // 4
        float humidity;     // 4
        uint8_t ctrl;       // 1
        // tot 15
    } th_protocol;

    typedef struct dt_protocol
    {
        uint16_t year;
        uint8_t month;
        uint8_t day;
        uint8_t hour;
        uint8_t minute;
        uint8_t second;
    } dt_protocol;

}