// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor


#include "TCPStream.hpp"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <sstream>

namespace snl
{

    TCPStream::TCPStream(int sd, sockaddr_in& address, std::shared_ptr<SnlLog> log) : _sd(sd), _log(log)
    {
        // https://www.linuxtoday.com/blog/blocking-and-non-blocking-i-0/
//        fcntl(sd, F_SETFL, fcntl(sd, F_GETFL) & ~O_NONBLOCK);

        // https://gist.github.com/xuesongbj/81911c7e72d67aa2ef54e62632d7d8a8
        char ip[INET_ADDRSTRLEN];
        ::inet_ntop(PF_INET, (in_addr*)&(address.sin_addr.s_addr), ip, INET_ADDRSTRLEN);
        _peerIP = ip;
        _peerPort = ntohs(address.sin_port);
    }

    TCPStream::~TCPStream()
    {
        ::close(_sd);
    }

    ssize_t TCPStream::send(const uint8_t* buffer, size_t len)
    {
        if (_disconnected)
        {
            return -1;
        }

        ssize_t send_size{-1};
        send_size = write(_sd, buffer, len);
        if (send_size == 0)
        {
            if (_log != nullptr) { _log->warn("connection reset by peer\n"); }
            _disconnected = true;
        }
        else if (send_size < 0)
        {
            if (_log != nullptr) { _log->error("error sending data. Error=%s\n", strerror(errno)); }
            _disconnected = true;
        }
        return send_size;
    }

    ssize_t TCPStream::receive(uint8_t* buffer, size_t len)
    {
        if (_disconnected)
        {
            return -1;
        }

        ssize_t recv_size{-1};
        recv_size = read(_sd, buffer, len);
        if (recv_size == 0)
        {
            if (_log != nullptr) { _log->warn("connection reset by peer\n"); }
            _disconnected = true;
        }
        else if (recv_size < 0)
        {
            if (errno == ECONNRESET)
            {
                if (_log != nullptr) { _log->warn("connection reset by peer\n"); }
            }
            else
            {
                if (_log != nullptr) { _log->error("error receiving data. Error=%s\n", strerror(errno)); }
            }
            _disconnected = true;
        }
        else if (recv_size < len)
        {
            if (_log != nullptr) { _log->warn("not all data received (%u/%u)\n", recv_size, len); }
        }
        return recv_size;
    }

    bool TCPStream::available()
    {
        pollfd fds{};
        fds.fd = _sd;
        fds.events |= POLLIN;
        if (poll(&fds, 1, 0) > 0)
        {
            if (fds.revents & POLLIN)
            {
                return true;
            }
        };

        return false;
/*        int count;
        int res = ioctl(_sd, FIONREAD, &count);
        if (res < 0)
        {
            return 0;
        }
        return count;
*/
    }

    std::string TCPStream::readStringUntil(char token)
    {
        std::stringstream ret;

        uint8_t c{0};
        ssize_t nr{0};
        nr = receive(&c, 1);

        while (nr > 0 && c != token)
        {
            ret << c;
            nr = receive(&c, 1);
        }

        return ret.str();
    }


/*    ssize_t TCPStream::receiveSNMHeader(SensorNetMessage &snm)
    {
        ssize_t rec_size{-1};

        rec_size = read(_sd, snm.data(), snm.header_length());

        if (rec_size != snm.header_length())
        {
            if (_logger != nullptr) { _logger->error("error receiving snm header. recv length != header length\n"); }
        }

        return rec_size;
    }

    ssize_t TCPStream::receiveSNMBody(SensorNetMessage &snm)
    {
        ssize_t rec_size{-1};

        rec_size = read(_sd, snm.body(), snm.body_length());

        if (rec_size != snm.body_length())
        {
            if (_logger != nullptr) { _logger->error("error receiving snm body. recv length != body length\n"); }
        }

        return rec_size;
    }
*/
    void TCPStream::setReceiveTimeout(time_t timeout_sec)
    {
        // https://manpages.debian.org/stable/manpages/socket.7.en.html
        timeval tv{0};
        tv.tv_sec = timeout_sec;  // 0 secs = no timeout
        setsockopt(_sd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));

    }

    void TCPStream::setSendTimeout(time_t timeout_sec)
    {
        // https://manpages.debian.org/bullseye/manpages/socket.7.en.html
        timeval tv{0};
        tv.tv_sec = timeout_sec;  // x secs timeout
        setsockopt(_sd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(struct timeval));

    }

    bool TCPStream::disconnected()
    {
        return _disconnected;
    }

}