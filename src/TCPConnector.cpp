// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#include "TCPConnector.hpp"

namespace snl
{
    TCPConnector::TCPConnector(std::shared_ptr<SnlLog> log) : _log(log)
    {

    }

    std::unique_ptr<TCPStream> TCPConnector::createTCPStream(int sd, sockaddr_in &address, std::shared_ptr<SnlLog> logger)
    {
        return std::unique_ptr<TCPStream>(new TCPStream(sd, address, logger));;
    }

}
