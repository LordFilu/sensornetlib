// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#include "TCPAcceptor.hpp"
#include "impl/TCPAcceptorPosix.hpp"
#include "SensorNetMessage.hpp"
#include "protocol.hpp"
#include "MessageBroker.hpp"

#include <memory>
#include <cstdarg>

class LogStd : public snl::SnlLog
{
public:
    void info(const char *__restrict fmt, ...) override
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };

    void warn(const char *__restrict fmt, ...) override
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };

    void error(const char *__restrict fmt, ...) override
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };


};

int main(int argc, char* argv[])
{
    std::shared_ptr<LogStd> logstd = std::make_shared<LogStd>();

    std::unique_ptr<snl::TCPAcceptor> acceptor = std::unique_ptr<snl::TCPAcceptorPosix>(new snl::TCPAcceptorPosix(44333, "", logstd));

    while (true)
    {
        // wait for connection (blocking call)
        std::unique_ptr<snl::TCPStream> stream = acceptor->listen();
        stream->setReceiveTimeout(3);
        stream->setSendTimeout(3);
        snl::MessageBroker mb(std::move(stream), logstd);
        while (!mb.disconnected())
        {
            std::unique_ptr<snl::th_protocol> thp = mb.receiveSensorData();
            if (thp != nullptr)
            {
                logstd->info("Receiceved data SNM (%u-%u-%u %u:%u T=%f°C H=%frH\n", thp->year, (uint16_t)thp->month, (uint16_t)thp->day, (uint16_t)thp->hour, (uint16_t)thp->minute, thp->temperature, thp->humidity);
            }
            else
            {
                if (mb.notAllDataReceived())
                {
                    logstd->error("error receiving data SNM\n");
                }
            }
        }

    }


    return 0;
}
