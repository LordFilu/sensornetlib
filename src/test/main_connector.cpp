// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#include "TCPConnector.hpp"
#include "impl/TCPConnectorPosix.hpp"
#include "SensorNetMessage.hpp"
#include "protocol.hpp"
#include "MessageBroker.hpp"

#include <memory>
#include <chrono>
#include <cstring>
#include <iostream>
#include <cstdarg>
#include <thread>

class LogStd : public snl::SnlLog
{
public:
    void info(const char *__restrict fmt, ...) override
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };

    virtual void warn(const char *__restrict fmt, ...)
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };

    virtual void error(const char *__restrict fmt, ...)
    {
        va_list argptr;
        va_start(argptr, fmt);
        vfprintf(stderr, fmt, argptr);
        va_end(argptr);
    };


};



int main(int argc, char* argv[])
{
    std::shared_ptr<LogStd> logstd = std::make_shared<LogStd>();

    std::unique_ptr<snl::TCPConnector> connector = std::unique_ptr<snl::TCPConnectorPosix>(new snl::TCPConnectorPosix(logstd));

    // connect
    std::unique_ptr<snl::TCPStream> stream = connector->connectToIP("192.168.92.47", 44333);

    if (stream != nullptr)
    {
        stream->setReceiveTimeout(3);
        stream->setSendTimeout(3);
        snl::MessageBroker mb(std::move(stream), logstd);

        for (uint8_t i = 0; i < 10; i++)
        {
            std::unique_ptr<snl::th_protocol> thp = std::unique_ptr<snl::th_protocol>(new snl::th_protocol());
            std::chrono::time_point<std::chrono::system_clock> ts_now = std::chrono::system_clock::now();
            time_t ttnow = std::chrono::system_clock::to_time_t(ts_now);
            struct tm * timeinfo;
            timeinfo = localtime(&ttnow);
            thp->temperature = 20;
            thp->humidity = 50;
            thp->minute = timeinfo->tm_min;
            thp->hour = timeinfo->tm_hour;
            thp->day = timeinfo->tm_mday;
            thp->month = timeinfo->tm_mon+1;
            thp->year = timeinfo->tm_year+1900;
            thp->ctrl = snl::VALID;

            mb.sendSensorData(std::move(thp));

            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1000ms);

        }


    }


    return 0;
}
