// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#include "TCPAcceptorPosix.hpp"

#include <unistd.h>
#include <netinet/in.h>
#include <iostream>
#include <cstring>

namespace snl
{
    TCPAcceptorPosix::TCPAcceptorPosix(uint16_t port, const std::string& ip_addr, std::shared_ptr<SnlLog> log) : TCPAcceptor(port, ip_addr, log)
    {
        // https://manpages.debian.org/bullseye/manpages/socket.7.en.html
        _lsd = socket(AF_INET, SOCK_STREAM, 0);
        if (_lsd <= 0)
        {
            if (_log != nullptr) { _log->error("error opening socket. errno=%s\n", strerror(errno)); }
            return;
        }

        memset((char *) &_serv_addr, 0, sizeof(_serv_addr));
        _serv_addr.sin_family = AF_INET;
        _serv_addr.sin_addr.s_addr = INADDR_ANY;
        _serv_addr.sin_port = htons(_port);
        if (bind(_lsd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0)
        {
            if (_log != nullptr) { _log->error("error on binding. errno=%s\n", strerror(errno)); }
            close(_lsd);
            _lsd = -1;
            return;
        }
    }

    std::unique_ptr<TCPStream> TCPAcceptorPosix::listen()
    {
        if (_lsd <= 0)
        {
            if (_log != nullptr) { _log->error("no valid socket file descriptor\n"); }
            return nullptr;
        }

        // https://manpages.debian.org/stable/manpages-dev/listen.2.en.html
        if (::listen(_lsd, 1) < 0)
        {
            if (_log != nullptr) { _log->error("error on listen. Errno=%s\n", strerror(errno)); }
            return nullptr;
        }

        sockaddr_in     cli_addr{0};
        socklen_t clilen = sizeof(cli_addr);

        // https://manpages.debian.org/bullseye/manpages-dev/accept.2.en.html
        int sd = ::accept(_lsd, (struct sockaddr *) &cli_addr, &clilen);
        if (sd < 0)
        {
            if (_log != nullptr) { _log->error("error on accept. Errno=%s\n", strerror(errno)); }
            return nullptr;
        }

        return createTCPStream(sd, cli_addr, _log);
    }

    TCPAcceptorPosix::~TCPAcceptorPosix()
    {
        if (_lsd > 0)
        {
            ::close(_lsd);
        }
    }

/*    bool TCPAcceptorPosix::start()
    {
        // https://manpages.debian.org/bullseye/manpages/socket.7.en.html
        _lsd = ::socket(AF_INET, SOCK_STREAM, 0);
        if (_lsd < 0)
        {
            std::cerr << "error opefning socket. errno=" << strerror(errno) << std::endl;
            return false;
        }

        ::memset((char *) &_serv_addr, 0, sizeof(_serv_addr));
        _serv_addr.sin_family = AF_INET;
        if (!_ip_addr.empty())
        {
            ::inet_pton(PF_INET, _ip_addr.c_str(), &(_serv_addr.sin_addr));
        }
        else
        {
            _serv_addr.sin_addr.s_addr = INADDR_ANY;
        }
        _serv_addr.sin_port = htons(_port);

        int optval = 1;
        ::setsockopt(_lsd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

        if (::bind(_lsd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0)
        {
            std::cerr << "error on binding. errno=" << strerror(errno) << std::endl;
            ::close(_lsd);
            _lsd = -1;
            return false;
        }

        return true;

//-------------------------------
*//*        _lsd = socket(PF_INET, SOCK_STREAM, 0);
        struct sockaddr_in address;

        memset(&address, 0, sizeof(address));
        address.sin_family = PF_INET;
        address.sin_port = htons(m_port);
        if (m_address.size() > 0) {
            inet_pton(PF_INET, m_address.c_str(), &(address.sin_addr));
        }
        else {
            address.sin_addr.s_addr = INADDR_ANY;
        }

        int optval = 1;
        setsockopt(m_lsd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

        int result = bind(m_lsd, (struct sockaddr*)&address, sizeof(address));
        if (result != 0) {
            perror("bind() failed");
            return result;
        }

        result = listen(m_lsd, 5);
        if (result != 0) {
            perror("listen() failed");
            return result;
        }
        m_listening = true;
        return result;
*/
/*
    }
*/

}
