// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#pragma once

#include "../TCPStream.hpp"
#include "../TCPAcceptor.hpp"

#include <memory>

namespace snl
{
    class TCPAcceptorPosix : public TCPAcceptor
    {
    public:
        TCPAcceptorPosix() = delete;
        TCPAcceptorPosix(uint16_t port, const std::string& ip_addr = "", std::shared_ptr<SnlLog> log = nullptr);
        ~TCPAcceptorPosix() override;

//        bool start();
        std::unique_ptr<TCPStream> listen();

    private:
    };

}

