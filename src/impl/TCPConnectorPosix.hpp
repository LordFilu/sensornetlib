// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor

#pragma once

#include "../TCPStream.hpp"
#include "../TCPConnector.hpp"

#include <memory>

namespace snl
{
    class TCPConnectorPosix : public TCPConnector
    {
    public:
        explicit TCPConnectorPosix(std::shared_ptr<SnlLog> log = nullptr);

        std::unique_ptr<TCPStream> connectToIP(const std::string& ip, uint16_t port, uint32_t timeout) override ;
        std::unique_ptr<TCPStream> connectToHost(const std::string& hostname, uint16_t port, uint32_t timeout) override;

    private:
        static int resolveHostName(const std::string& hostname, in_addr& addr);
        std::unique_ptr<TCPStream> connect(in_addr address, uint16_t port, uint32_t timeout = 0);
    };

}

