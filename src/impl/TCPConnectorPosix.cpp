// (c) 2023 by SwordLord - the coding crew
// sensor logger project

#include "TCPConnectorPosix.hpp"

#include <memory>
#include <iostream>
#include <cstring>

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>

namespace snl
{
    TCPConnectorPosix::TCPConnectorPosix(std::shared_ptr<SnlLog> log) : TCPConnector(log)
    {

    }

    std::unique_ptr<TCPStream> TCPConnectorPosix::connectToIP(const std::string &ip, uint16_t port, uint32_t timeout)
    {
        in_addr address{0};
        if (inet_pton(AF_INET, ip.c_str(), &address) == 1)
        {
            return connect(address, port, timeout);

        }

        return nullptr;

    }

    std::unique_ptr<TCPStream> TCPConnectorPosix::connectToHost(const std::string &hostname, uint16_t port, uint32_t timeout)
    {
        in_addr address{0};
        if (resolveHostName(hostname, address) == 0 )
        {
            return connect(address, port, timeout);
        }
        else
        {
            if (_log != nullptr) { _log->error("could not resolve hostname %s\n", hostname.c_str()); }
        }
        return nullptr;

    }

    int TCPConnectorPosix::resolveHostName(const std::string& hostname, in_addr& addr)
    {
        struct addrinfo *res;

        int result = ::getaddrinfo(hostname.c_str(), NULL, NULL, &res);
        if (result == 0)
        {
            memcpy(&addr, &((sockaddr_in *) res->ai_addr)->sin_addr, sizeof(struct in_addr));
            freeaddrinfo(res);
        }
        return result;
    }

    std::unique_ptr<TCPStream> TCPConnectorPosix::connect(in_addr address, uint16_t port, uint32_t timeout)
    {
        int sd = ::socket(AF_INET, SOCK_STREAM, 0);
        if (sd < 0)
        {
            if (_log != nullptr) { _log->error("error opening socket. errno=%s\n", strerror(errno)); }
            return nullptr;
        }

        ::sockaddr_in serv_addr{0};

        memset((char *) &serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(port);
        serv_addr.sin_addr = address;

        if (::connect(sd, (sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        {
            if (_log != nullptr) { _log->error("error connecting to server. Errno=%s\n", strerror(errno)); }
            ::close(sd);
            return nullptr;
        }

        return createTCPStream(sd, serv_addr, _log);
    }

}