// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor


#pragma once

#include "protocol.hpp"

#include <string>
#include <memory>

namespace snl
{

    class SensorNetMessage
    {
    public:
        // sensor message types
        static const uint8_t REQUESTDATA[2]; //{'D', 'R'}
        static const uint8_t DATAMSG[2];// {'D', 'M'}
        static const uint8_t RESENDDATAMSG[2];// {'R', 'M'}
        static const uint8_t SNMOK[2];// {'S', 'O'}

    public:
        static constexpr uint8_t HEADER_SIZE_LENGTH = 2;
        static constexpr uint8_t HEADER_TYPE_LENGTH = 2;
        static constexpr uint16_t MAX_BODY_LENGTH = 512;

        SensorNetMessage();
        SensorNetMessage(uint8_t t1, uint8_t t2);
        explicit SensorNetMessage(const uint8_t (&type)[2]);

        uint8_t* data() { return _data; }
        std::size_t length() const { return HEADER_SIZE_LENGTH + HEADER_TYPE_LENGTH + _body_length; }
        const uint8_t* body() const { return _data + HEADER_SIZE_LENGTH + HEADER_TYPE_LENGTH; }
        uint8_t* body() { return _data + HEADER_SIZE_LENGTH + HEADER_TYPE_LENGTH; }
        uint16_t body_length() const { return _body_length; }
        uint16_t header_length() const { return HEADER_SIZE_LENGTH + HEADER_TYPE_LENGTH; }
        std::string message() const;

        bool     typeEqualTo(char t1, char t2) const;
        bool     typeEqualTo(const uint8_t (&tp)[2]) const;

        void body_length(std::size_t new_length);
        bool decode_header();
        void encode_header();
        void clear();

        std::unique_ptr<th_protocol>    evalToThp();
        void                            evalToSNM(std::unique_ptr<th_protocol> thp);

    private:
        uint8_t     _type[2]{};
        uint16_t    _body_length{0};
        uint8_t       _data[HEADER_SIZE_LENGTH + HEADER_TYPE_LENGTH + MAX_BODY_LENGTH] = { 0 };

    };

}


