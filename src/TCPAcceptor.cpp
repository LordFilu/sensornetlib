// (c) 2023 by SwordLord - the coding crew
// sensor logger project

#include "TCPAcceptor.hpp"

namespace snl
{

    TCPAcceptor::TCPAcceptor(uint16_t port, const std::string& ip_addr, std::shared_ptr<SnlLog> log) : _port(port), _ip_addr(ip_addr), _log(log)
    {
    }


    std::unique_ptr<TCPStream> TCPAcceptor::createTCPStream(int sd, sockaddr_in& address, std::shared_ptr<SnlLog> logger)
    {
        return std::unique_ptr<TCPStream>(new TCPStream(sd, address, logger));
    }

}
