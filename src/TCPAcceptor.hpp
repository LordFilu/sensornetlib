// (c) 2023 by SwordLord - the coding crew
// sensor logger project
// hw:      Adafruit ESP32-S3 Feather 8MB Flash No PSRAM
// sensor:  Adafruit Sensirion SHT40 Temperature & Humidity Sensor


#pragma once

#include "TCPStream.hpp"
#include "SnlLog.hpp"

#include <memory>

namespace snl
{
    class TCPAcceptor
    {
    public:
        TCPAcceptor(uint16_t port, const std::string& ip_addr = "", std::shared_ptr<SnlLog> log = nullptr);
        virtual ~TCPAcceptor() = default;

        virtual std::unique_ptr<TCPStream> listen() = 0;

    protected:
        int             _lsd{-1};
        uint16_t        _port{0};
        std::string     _ip_addr{""};
        bool            _listening{false};
        sockaddr_in     _serv_addr{0};

        static std::unique_ptr<TCPStream> createTCPStream(int sd, sockaddr_in& address, std::shared_ptr<SnlLog> logger);
        std::shared_ptr<SnlLog> _log;
    };

}


