# Credits

#### Net design patterns
Inspired by [vichargrave.github.io](https://vichargrave.github.io/programming/tcp-ip-network-programming-design-patterns-in-cpp/)

#### Network message
Heavily based on [chat_message from asio](https://github.com/chriskohlhoff/asio)

